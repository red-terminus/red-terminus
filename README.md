# Red Terminus

**RedTerminus** é uma plataforma para organizar e gerenciar acesso a máquinas físicas, VMs e instâncias em cloud.

## Features (Requisitos)

### Implementadas

### Em planejamento

* Login
* Adicionar informações de acesso a máquinas via SSH.
	* usuário
	* senha
	* ip/hostname
	* chave privada
	* senha para chave privada
	* argumentos adicionais

* Rotina para validar se as credenciais de acesso são válidas
	* Salvar data e hora de tentativa de acesso com resultado se funcionou ou não
* Página para visualizar tentativas de acesso

### Em desenvolvimento

### Descontinuadas

## Requisitos não funcionais

### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all Node dependencies pre-installed and Express will open a web preview.
